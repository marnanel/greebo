#!/usr/bin/env python3

import argparse
import logging
import xml.sax
import re
import sys
import textwrap

PAGE_HEADER_FORMAT = '==__MATCH__:[[%(filename)s/%(page)s]]==\n'

TAGLIST = [
        'ab', 'above', 'abbr', 'add', 'altbibno', 'argument', 'back', 'below',
        'bibl', 'bibno', 'body', 'byline', 'cell', 'choice', 'closer', 'date',
        'dateline', 'del', 'div1', 'div2', 'div3', 'div4', 'div5', 'div6',
        'div7', 'eebo', 'epigraph', 'ets', 'expan', 'figDesc', 'figure',
        'floatext', 'front', 'fw', 'gap', 'group', 'head', 'header',
        'headnote', 'hi', 'idg', 'item', 'l', 'label', 'lb', 'letter',
        'lg', 'license', 'list', 'milestone', 'note', 'opener', 'p', 'pb',
        'postscript', 'ptr', 'q', 'ref', 'row', 'salute', 'seg', 'signed',
        'sp', 'speaker', 'stage', 'stc', 'sub', 'subst', 'sup', 'table',
        'tailnote', 'temphead', 'text', 'trailer', 'unclear', 'vid', 
        ]

class EeboHandler(xml.sax.handler.ContentHandler):

    def __init__(self,
            output,
            *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.output = output
        self.div_level = 0
        self.in_header_or_trailer = False

    def startElement(self, name, attrs):
        logging.debug('START: %s %s', name, dict(attrs))

        if name in ['HEADER', 'TRAILER']:
            self.in_header_or_trailer = True
            return

        if self.in_header_or_trailer:
            # Nothing in the header is useful to us,
            # and it contains weird tags which break
            # our checks.
            return

        if name.lower() not in TAGLIST:
            raise ValueError(f"Tag {name} is not in the EEBO spec")

        if name=='PB':
            self.output.start_page(
                    page=attrs['REF'],
                    )
        elif name.startswith('DIV'):
            self.div_level += 1
        elif name=='P':
            self.output.start_para(
                    numbered = 'N' in attrs,
                    )
        elif name=='GAP':
            # Difficult to know what to do here.
            # Check the DESC attribute for a clue.
            # Meanwhile, here's a simple workaround
            self.output.write(['[%s]' % (attrs['DESC'],)])
        elif name=='HEAD':
            self.output.start_para(
                    heading = True,
                    )
        elif name in ['MILESTONE', 'ABBR', 'HI', 'FIGURE']:
            pass # nothing very useful afaics
        elif name=='NOTE':
            if attrs['PLACE'] == 'marg':
                self.output.start_span(
                        marginalia = True,
                        )
            else:
                raise ValueError("Unknown note type: "+attrs['PLACE'])
        elif name=='SUP':
            self.output.start_literal_tag('sup')
        else:
            if self.div_level!=0:
                raise ValueError(
                        f"Did not expect tag {name} in the text block")

    def endElement(self, name):
        logging.debug('END  : %s', name)

        if name in ['HEADER', 'TRAILER']:
            self.in_header_or_trailer = False
            return

        if self.in_header_or_trailer:
            return

        if name in ['P', 'HEAD']:
            self.output.end_para()
        elif name=='NOTE':
            self.output.end_span()
        elif name=='SUP':
            self.output.end_literal_tag('sup')
        elif name.startswith('DIV'):
            self.div_level -= 1

    def characters(self, content):

        logging.debug('CHARS: %s', content)

        if self.div_level==0:
            return

        content = content.strip().split()
        if not content:
            return

        self.output.write(content)

    def startDocument(self):
        logging.debug('START DOC')

    def endDocument(self):
        logging.debug('END DOC')

class WikitextOutput:

    def __init__(self, target, filename):
        self.target = target
        self.buffer = []
        self.prefix = False
        self.filename = filename

    def start_page(self, page):
        self.target.write(PAGE_HEADER_FORMAT % {
            'filename': self.filename,
            'page': page,
            })

    def write(self, words):

        if self.prefix:
            self.buffer[-1] += words[0]
            words = words[1:]
            self.prefix = False

        self.append(words)

    def start_para(self,
            numbered = False,
            heading = False,
            ):

        # Don't clear the buffer here. If someone nests <P>,
        # it will result in losing text.

        if numbered:
            self.append('#')

        if heading:
            self.append('==')

    def start_span(
            self,
            marginalia = True,
            ):

        if marginalia:
            self.append('{{Marginalia|',
                    prefix = True)
        else:
            raise ValueError("Unknown span type")

    def start_literal_tag(
            self,
            tag):
        self.append(f'<{tag}>',
                prefix = True)

    def end_literal_tag(
            self,
            tag):
        self.append(f'</{tag}>',
                keep_slashes = True,
                suffix = True)

    def append(self,
            text,
            prefix = False,
            suffix = False,
            keep_slashes = False):

        if type(text)==list:
            for line in text:
                self.append(line, prefix, suffix)
            return

        if not keep_slashes:
            text = text.replace('/', '\n', -1)

        if prefix:
            self.prefix = True

        if suffix:
            if self.buffer:
                self.buffer[-1] += text
                return
        
        self.buffer.append(text)

    def end_span(self):
        self.append('}}',
                suffix = True)

    def end_para(self):
        text = ' '.join(self.buffer)

        lines = text.split('\n')

        for line in lines:
            logging.debug('OUTPUT: %s',
                    line)

            wrapped_lines = textwrap.wrap(line.strip())

            for wrapped_line in wrapped_lines:
                self.target.write('%s\n' % (
                    wrapped_line,
                    ))

        self.buffer = []

def main():

    parser = argparse.ArgumentParser(description='')
    parser.add_argument('eebo',
            help='EEBO file to parse')
    parser.add_argument('--verbose', '-v', action="store_true",
            help='show debugging output')
    parser.add_argument('--output', '-o',
            action='store',
            default=sys.stdout,
            help='output target (default is stdout)')
    args = parser.parse_args()

    log_level = logging.DEBUG if args.verbose else logging.INFO
    logging.basicConfig(level=log_level)

    parser = xml.sax.make_parser()

    with open(args.eebo) as f:
        output = WikitextOutput(args.output,
                filename = f.name,
                )
        handler = EeboHandler(
                output = output,
                )
        parser.setContentHandler(handler)

        parser.parse(f)

if __name__ == "__main__":
    main()
